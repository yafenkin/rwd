import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AdsService } from "../ads.service";
import { Statuses } from "./ads.model";

@Component({
  templateUrl: "./ads-activation.component.html",
  styleUrls: ["ads-activation.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AdsActivationComponent implements OnInit {
  id: string;
  statuses: Statuses;

  constructor(private adsService: AdsService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.adsService.getAdStatuses(this.id).toPromise()
    .then(statuses => {
      this.statuses = statuses;
    })
  }

  setStatus(adType: string, state: number): void {
    this.adsService.setActivationStatus(this.id, adType, state).toPromise()
    .then(res => {
      if (res === 1) {
        this.statuses[adType] = state;
      }
    })
  }
}
