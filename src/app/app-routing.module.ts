import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { BoardComponent } from "./board/board.component";
import { BoardlistComponent } from "./boardlist/boardlist.component";
import { ClubDetailsBlueprintsComponent } from "./club-details-blueprints/club-details-blueprints.component";
import { ClubDetailsWrapperComponent } from "./club-details-wrapper/club-details-wrapper.component";
import { ClubsComponent } from "./clubs/clubs.component";
import { AdsActivationComponent } from "./creds/ads-activation.components";
import { GranPrixGroupComponent } from "./granprix-group/granprix-group.component";
import { GranPrixBoardComponent } from "./granprix/granprix-board.component";
import { MembersWrapperComponent } from "./members-wrapper/members-wrapper.component";

const routes: Routes = [
  {
    path: "boards",
    component: BoardlistComponent,
  },
  {
    path: "boards/:id/:platform",
    component: BoardComponent,
  },
  {
    path: "clubs/:platform",
    component: ClubsComponent,
  },
  {
    path: "clubs/:platform/:id",
    component: ClubDetailsWrapperComponent,
    pathMatch: "full",
  },
  {
    path: "clubs/:platform/:clubUuid/blueprints",
    component: ClubDetailsBlueprintsComponent,
    pathMatch: "full",
  },
  {
    path: "members/:platform",
    component: MembersWrapperComponent,
  },
  {
    path: "gp/:platform/:group/info",
    component: GranPrixGroupComponent,
  },
  {
    path: "gp/:stage/:platform/:group/:round",
    component: GranPrixBoardComponent,
  },
  {
    path: "ads/:id",
    component: AdsActivationComponent
  },
  {
    path: "**",
    redirectTo: "boards",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
