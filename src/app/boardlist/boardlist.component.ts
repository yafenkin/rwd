import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { BoardsService } from '../boards.service';

@Component({
  selector: 'app-boardlist',
  templateUrl: './boardlist.component.html',
  styleUrls: ["boardlist.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class BoardlistComponent implements OnInit {

  private static ACTIVE_TAB_KEY: string = "activeTab";

  boards: Observable<any>;
  activeTabIndex: number = 0;

  constructor(private boardsService: BoardsService) {
  }

  ngOnInit(): void {
    this.boards = this.boardsService.getBoards();
    this.activeTabIndex = +sessionStorage.getItem(BoardlistComponent.ACTIVE_TAB_KEY) || 0;
  }

  handleTabChange($event): void {
    this.activeTabIndex = $event.index;
    sessionStorage.setItem(BoardlistComponent.ACTIVE_TAB_KEY, $event.index);
  }
}
