import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BoardsService } from '../boards.service';

@Component({
  selector: "better-board",
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BoardComponent implements OnInit {

  data: object;
  offset: string;
  platform: string;
  autorefreshEnabled: boolean;
  timer: any;
  numbersRegex: RegExp = /^\d+$/;

  constructor(private boardsService: BoardsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.platform = this.route.snapshot.params.platform;
    this.data = this.boardsService.getBoard(this.route.snapshot.params.id, this.platform);
  }

  toggleAutorefresh($event): void {
    if (this.offset && !this.numbersRegex.test(this.offset))
      return;

    if ($event.checked) {
      this.timer = setInterval((): void => {
        this.data = this.boardsService.getBoard(this.route.snapshot.params.id, this.platform, this.offset);
      }, 10000);
    } else {
      clearInterval(this.timer);
    }
  }

  onBlur(): void {
    if (this.offset && !this.numbersRegex.test(this.offset))
      return;

    this.data = this.boardsService.getBoard(this.route.snapshot.params.id, this.platform, this.offset);
  }
}
