import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/button';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from "primeng/table";
import { TabViewModule } from 'primeng/tabview';
import { AdsService } from './ads.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { BoardlistComponent } from './boardlist/boardlist.component';
import { BoardsService } from './boards.service';
import { ClubDetailsBlueprintsComponent } from './club-details-blueprints/club-details-blueprints.component';
import { ClubDetailsWrapperComponent } from './club-details-wrapper/club-details-wrapper.component';
import { ClubDetailsComponent } from './club-details/club-details.component';
import { ClubsComponent } from './clubs/clubs.component';
import { AdsActivationComponent } from './creds/ads-activation.components';
import { GranPrixGroupComponent } from './granprix-group/granprix-group.component';
import { GranPrixBoardComponent } from './granprix/granprix-board.component';
import { MembersWrapperComponent } from './members-wrapper/members-wrapper.component';

@NgModule({
  declarations: [
    AdsActivationComponent,
    AppComponent,
    BoardComponent,
    BoardlistComponent,
    ClubsComponent,
    ClubDetailsComponent,
    ClubDetailsBlueprintsComponent,
    ClubDetailsWrapperComponent,
    GranPrixBoardComponent,
    GranPrixGroupComponent,
    MembersWrapperComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ButtonModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    InputSwitchModule,
    ReactiveFormsModule,
    TableModule,
    TabViewModule,
  ],
  providers: [
    AdsService,
    BoardsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
