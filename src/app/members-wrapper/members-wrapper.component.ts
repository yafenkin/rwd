import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BoardsService } from '../boards.service';

@Component({
    selector: 'members-wrapper',
    templateUrl: './members-wrapper.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MembersWrapperComponent implements OnInit {

    members: Observable<any>;

    constructor(private boardsService: BoardsService, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.members = this.boardsService.getMemberLeaderboards(this.route.snapshot.params.platform);
    }
}
