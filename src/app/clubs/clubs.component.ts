import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BoardsService } from '../boards.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'clubs',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClubsComponent implements OnInit {

  clubs: Observable<any>;

  constructor(private boardsService: BoardsService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.clubs = this.boardsService.getClubLeaderboards(this.route.snapshot.params.platform);
  }
}
