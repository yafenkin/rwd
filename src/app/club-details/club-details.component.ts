import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { BoardsService } from '../boards.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'club-details',
  templateUrl: './club-details.component.html',
  styleUrls: ['./club-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClubDetailsComponent {
  @Input()
  club: any;
}
