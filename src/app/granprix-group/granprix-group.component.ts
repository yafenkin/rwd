import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { BoardsService } from "../boards.service";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  templateUrl: "./granprix-group.component.html",
  styleUrls: ["granprix-group.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class GranPrixGroupComponent implements OnInit {
  data: Observable<any>;
  platform: string;
  group: number;

  constructor(
    private boardsService: BoardsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.platform = this.route.snapshot.params.platform;
    this.group = +this.route.snapshot.params.group;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.data = this.boardsService.getGranPrixGroupInfo(
      this.route.snapshot.params.platform,
      this.route.snapshot.params.group
    );
  }

  previousGroup(): void {
    this.router.navigate(["gp", this.platform, this.group - 1, "info"]);
  }

  nextGroup(): void {
    this.router.navigate(["gp", this.platform, this.group + 1, "info"]);
  }

  openFinalsBoard(): void {
    this.router.navigate(["gp", "f", this.platform, this.group, 1]);
  }
}
