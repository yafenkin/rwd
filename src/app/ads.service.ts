import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Statuses } from "./creds/ads.model";

@Injectable()
export class AdsService {
  constructor(private readonly http: HttpClient) {}

  getAdStatuses(id: string): Observable<Statuses> {
    return this.http.get<Statuses>(`/sapi/creds/${id}`);
  }

  setActivationStatus(id: string, type: string, status: number): Observable<number> {
    let holder = {};
    holder[type] = status;
    return this.http.post<number>(`/sapi/creds/${id}`, holder);
  }
}
