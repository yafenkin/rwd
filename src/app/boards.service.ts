import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class BoardsService {
  constructor(private readonly http: HttpClient) {}

  getBoards(): Observable<any> {
    return this.http.get<any>("/sapi/boards");
  }

  getBoard(
    id: string,
    platform: string,
    offset: string = "0"
  ): Observable<any> {
    return this.http.get(`/api/${platform}/boards/${id}?offset=${offset}`);
  }

  getEliteBoards(): Observable<any> {
    return this.http.get(`/api/android/elite`);
  }

  getClubLeaderboards(platform: string): Observable<any> {
    return this.http.get(`/api/${platform}/clubs`);
  }

  getClubDetails(id: string, platform: string): Observable<any> {
    return this.http.get(`/api/${platform}/clubs/${id}`);
  }

  getMemberLeaderboards(platform: string): Observable<any> {
    return this.http.get(`/api/${platform}/members`);
  }

  getGranPrixGroupInfo(platform: string, group: number): Observable<any> {
    return this.http.get(`/api/${platform}/gp/${group}/info`);
  }

  getGranPrixBoard(
    stage: string,
    platform: string,
    id: number,
    round: number
  ): Observable<any> {
    return this.http.get(`/api/${platform}/gp/${stage}/${id}/${round}`);
  }

  getClubBlueprintData(platform: string, clubUuid: string): Observable<any> {
    return this.http.get(`/api/${platform}/sebp/${clubUuid}`);
  }
}
