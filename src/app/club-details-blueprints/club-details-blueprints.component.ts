import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { BoardsService } from "../boards.service";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { Location } from "@angular/common";

@Component({
  templateUrl: "./club-details-blueprints.component.html",
  styleUrls: ["./club-details-blueprints.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class ClubDetailsBlueprintsComponent implements OnInit {
  blueprintData: Observable<any>;

  constructor(
    private boardsService: BoardsService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.blueprintData = this.boardsService.getClubBlueprintData(
      this.route.snapshot.params.platform,
      this.route.snapshot.params.clubUuid
    );
  }

  goBack(): void {
    this.location.back();
  }
}
