import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { BoardsService } from "../boards.service";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "better-board",
  templateUrl: "./granprix-board.component.html",
  styleUrls: ["granprix-board.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class GranPrixBoardComponent implements OnInit {
  data: Observable<any>;
  platform: string;
  group: number;
  round: number;
  stage: string;

  constructor(
    private boardsService: BoardsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.platform = this.route.snapshot.params.platform;
    this.group = +this.route.snapshot.params.group;
    this.round = +this.route.snapshot.params.round;
    this.stage = this.route.snapshot.params.stage;
  }

  ngOnInit(): void {
    this.data = this.boardsService.getGranPrixBoard(
      this.stage,
      this.platform,
      this.group,
      this.round
    );
  }

  nextGroup(): void {
    this.router.navigate([
      "gp",
      this.stage,
      this.platform,
      this.group + 1,
      this.round,
    ]);
  }

  nextRound(): void {
    this.router.navigate([
      "gp",
      this.stage,
      this.platform,
      this.group,
      this.round + 1,
    ]);
  }

  previousGroup(): void {
    this.router.navigate([
      "gp",
      this.stage,
      this.platform,
      this.group - 1,
      this.round,
    ]);
  }

  previousRound(): void {
    this.router.navigate([
      "gp",
      this.stage,
      this.platform,
      this.group,
      this.round - 1,
    ]);
  }

  goToInfo(): void {
    this.router.navigate(["gp", this.platform, this.group, "info"]);
  }
}
