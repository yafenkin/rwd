import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BoardsService } from '../boards.service';

@Component({
  selector: 'club-details-wrapper',
  templateUrl: './club-details-wrapper.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ClubDetailsWrapperComponent implements OnInit {

  platform: string;
  clubUuid: string;
  club: Observable<any>;

  constructor(private boardsService: BoardsService, private route: ActivatedRoute, private location: Location) {
  }

  ngOnInit(): void {
    this.platform = this.route.snapshot.params.platform;
    this.clubUuid = this.route.snapshot.params.id;
    this.club = this.boardsService.getClubDetails(this.clubUuid, this.platform);
  }

  goBack(): void {
    this.location.back();
  }
}
